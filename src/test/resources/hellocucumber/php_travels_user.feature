Feature: PHP Travels front end
  Everybody wants to book tickets through PHP travels

  Scenario: User Login
    Given I visit "https://www.phptravels.net" website
    When I select login button
    And I enter "user@phptravels.com" as "email"
    And I enter "demouser" as "password"
    And I click on login button
    Then I see "Hi, Demo User" text on page