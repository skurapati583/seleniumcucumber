package hellocucumber;

import io.cucumber.java.After;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class StepDefinitions {

    WebDriver driver;
    WebDriverWait wait;

    public StepDefinitions() {
        System.setProperty("webdriver.chrome.driver","webdrivers/chromedriver");
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 30);
    }

    @Given("I visit {string} website")
    public void i_visit_website(String url) {
        driver.get(url);
    }
    @When("I select login button")
    public void i_select_login_button() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".dropdown-login")));
        driver.findElement(By.cssSelector(".dropdown-login")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".dropdown-login > .dropdown-menu-right > div > a:first-child")));
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".dropdown-login > .dropdown-menu-right > div > a:first-child")));
        driver.findElement(By.cssSelector(".dropdown-login > .dropdown-menu-right > div > a:first-child")).click();
    }
    @When("I enter {string} as {string}")
    public void i_enter_as(String text, String type) {
        switch (type) {
            case "email":
                wait.until(ExpectedConditions.presenceOfElementLocated(By.name("username")));
                driver.findElement(By.name("username")).sendKeys(text);
                break;
            case "password":
                wait.until(ExpectedConditions.presenceOfElementLocated(By.name("password")));
                driver.findElement(By.name("password")).sendKeys(text);
                break;
        }
    }
    @When("I click on login button")
    public void when_i_click_on_button() {
        driver.findElement(By.cssSelector("button.loginbtn")).click();
    }
    @Then("I see {string} text on page")
    public void i_see_text_on_page(String text) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".text-align-left")));
        Assert.assertEquals("Hi, Demo User", driver.findElement(By.cssSelector(".text-align-left")).getText());
    }

    @After
    public void afterExec(Scenario scenario) {
        if(driver!=null) {
            driver.quit();
        }
    }

}
